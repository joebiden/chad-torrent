use serde::{de::DeserializeOwned, Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum State {
    Active,
    Paused,
    Queued,
    Checking,
    Error,
    Other,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Options {
    pub name: Option<String>,
    pub label: Option<String>,
    pub save_path: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Torrent {
    pub name: String,
    pub id: String,
    pub label: Option<String>,
    pub eta: i64,
    pub progress: f64,
    pub download_speed: f64,
    pub upload_speed: f64,
    pub num_peers: i64,
    pub save_path: String,
    pub size: i64,
    pub downloaded: i64,
    pub state: State,
}

/*
pub trait Torrent: std::fmt::Debug + Clone {
    fn name(&self) -> &str;
    fn id(&self) -> String;
    fn label(&self) -> Option<&str>;
    fn eta(&self) -> i64;
    /// Progress of the download, floating number between 0 and 1
    fn progress(&self) -> f64;
    /// Download speed in Bytes per second
    fn download_speed(&self) -> f64;
    /// Upload speed in Bytes per second
    fn upload_speed(&self) -> f64;
    fn num_peers(&self) -> i64;
    fn save_path(&self) -> &str;
    /// Total size in bytes
    fn size(&self) -> i64;
    /// Amount downloaded in bytes
    fn downloaded(&self) -> i64;
    /// Current state
    fn state(&self) -> State;
}*/
