use super::{Torrent, TorrentBackend};
use crate::torrent::State;
use anyhow::{anyhow, Context, Result};
use async_trait::async_trait;
use qbittorrent::{data::Hash, traits::*, Api};

impl Into<qbittorrent::queries::TorrentDownloadBuilder> for crate::torrent::Options {
    fn into(self) -> qbittorrent::queries::TorrentDownloadBuilder {
        let mut builder = &mut qbittorrent::queries::TorrentDownloadBuilder::default();

        if let Some(name) = self.name {
            builder = builder.rename(&name);
        }

        if let Some(label) = self.label {
            builder = builder.category(&label);
        }

        if let Some(save_path) = self.save_path {
            builder = builder.savepath(&save_path);
        }

        builder.clone()
    }
}

pub struct QBittorrentBackend {
    session: Api,
}

/*
pub struct QBittorrentTorrent {
    save_path: String,
    creation_date: i64,
    piece_size: i64,
    comment: String,
    total_wasted: i64,
    total_uploaded: i64,
    total_uploaded_session: i64,
    total_downloaded: i64,
    total_downloaded_session: i64,
    up_limit: i64,
    dl_limit: i64,
    time_elapsed: i64,
    seeding_time: i64,
    nb_connections: i64,
    nb_connections_limit: i64,
    share_ratio: f64,
    addition_date: i64,
    completion_date: i64,
    created_by: String,
    dl_speed_avg: i64,
    dl_speed: i64,
    eta: i64,
    last_seen: i64,
    peers: i64,
    peers_total: i64,
    pieces_have: i64,
    pieces_num: i64,
    reannounce: i64,
    seeds: i64,
    seeds_total: i64,
    total_size: i64,
    up_speed_avg: i64,
    up_speed: i64,
}
*/

impl Into<Torrent> for qbittorrent::data::Torrent {
    fn into(self) -> Torrent {
        Torrent {
            name: self.name().into(),
            id: self.hash().as_str().into(),
            label: Some(self.category().into()),
            eta: *self.eta(),
            progress: *self.progress(),
            download_speed: (*self.dlspeed()) as f64,
            upload_speed: (*self.upspeed()) as f64,
            num_peers: *self.num_seeds(),
            save_path: self.save_path().as_str().into(),
            size: *self.total_size(),
            downloaded: *self.downloaded(),
            state: {
                use qbittorrent::data::State as QState;
                match self.state() {
                    QState::Error | QState::MissingFiles => State::Error,
                    QState::CheckingUP | QState::CheckingDL | QState::CheckingResumeData => {
                        State::Checking
                    }
                    QState::PausedUP | QState::PausedDL => State::Paused,
                    QState::Downloading
                    | QState::Uploading
                    | QState::StalledUP
                    | QState::StalledDL
                    | QState::Allocating
                    | QState::MetaDL
                    | QState::Moving
                    | QState::ForcedUP
                    | QState::ForceDL => State::Active,
                    QState::QueuedUP | QState::QueuedDL => State::Queued,
                    QState::Unknown => State::Other,
                }
            },
        }
    }
}

impl QBittorrentBackend {
    pub async fn new(address: &str, username: &str, password: &str) -> Result<Self> {
        let session = Api::new(username, password, address).await?;
        Ok(Self { session })
    }
}

#[async_trait]
impl TorrentBackend for QBittorrentBackend {
    async fn list(&self, filter_label: Option<&str>) -> Result<Vec<Torrent>> {
        Ok(self
            .session
            .get_torrent_list()
            .await?
            .into_iter()
            .filter(|t| filter_label.is_none() || Some(t.category().as_str()) == filter_label)
            .map(|t| t.into())
            .collect())
    }

    async fn pause(&self, torrent_id: &str) -> Result<()> {
        qbittorrent::data::Hash::from(torrent_id.to_string())
            .pause(&self.session)
            .await
            .context("Failed to pause")
    }

    async fn resume(&self, torrent_id: &str) -> Result<()> {
        qbittorrent::data::Hash::from(torrent_id.to_string())
            .resume(&self.session)
            .await
            .context("Failed to resume")
    }

    async fn add_magnet(&self, magnet: &str, options: crate::torrent::Options) -> Result<String> {
        let mut builder: qbittorrent::queries::TorrentDownloadBuilder = options.into();
        builder
            .urls(magnet)
            .build()
            .map_err(|_| anyhow!("Failed to build query"))?
            .download(&self.session)
            .await
            .context("Failed to add magnet")
            .map(|_| String::from("").into()) // TODO Return the right hash
    }

    async fn remove_torrent(&self, torrent_id: &str, remove_data: bool) -> Result<()> {
        Err(anyhow!("Not implemented"))
    }

    async fn torrent(&self, torrent_id: &str) -> Result<Torrent> {
        self.session
            .get_torrent_list()
            .await?
            .into_iter()
            .filter(|t| t.hash().as_str() == torrent_id)
            .map(|t| t.into())
            .next()
            .ok_or(anyhow!("Torrent not found"))
    }
}
