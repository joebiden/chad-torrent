use crate::{Torrent, TorrentBackend};
use std::error::Error;
use async_trait::async_trait;

/*
pub struct MockTorrent {
    name: String,
}

impl MockTorrent {
    pub fn new<S: Into<String>>(name: S) -> Self {
        Self { name: name.into() }
    }
}

impl Torrent for MockTorrent {
    fn name(&self) -> &str {
        &self.name
    }
    
    fn hash(&self) -> &str {
        &self.name
    }
}

pub struct MockBackend {
    torrents: Vec<MockTorrent>,
}

impl MockBackend {
    pub fn new(torrents: Vec<MockTorrent>) -> Self {
        Self {
            torrents,
        }
    }
}

#[async_trait]
impl TorrentBackend<usize, MockTorrent> for MockBackend {
    async fn connect(_address: &str, _username: &str, _password: &str) -> Result<(), Box<dyn Error>> {
         Ok(())
    }

    async fn list(&self) -> Vec<&MockTorrent> {
        self.torrents.iter().collect() 
    }
    
    async fn pause(&self, hash: &str) -> Result<(), Box<dyn Error>> { Ok(()) }
    async fn resume(&self, hash: &str) -> Result<(), Box<dyn Error>> { Ok(()) }
    async fn add_magnet(&self, magnet: &str) -> Result<(), Box<dyn Error>> { Ok(()) }
    async fn remove_torrent(&self, hash: &str, remove_data: bool) -> Result<(), Box<dyn Error>> { Ok(()) }
}
*/
