pub mod deluge;
mod mock;
pub mod qbittorrent;

pub use self::qbittorrent::QBittorrentBackend;
pub use deluge::DelugeBackend;

use crate::torrent::{Options, Torrent};
use anyhow::Result;
use async_trait::async_trait;
use enum_dispatch::enum_dispatch;

#[enum_dispatch(TorrentBackend)]
pub enum TorrentClient {
    QBittorrentBackend,
    DelugeBackend,
}

#[async_trait]
#[enum_dispatch]
pub trait TorrentBackend {
    async fn list(&self, filter_labels: Option<&str>) -> Result<Vec<Torrent>>;
    async fn pause(&self, torrent_id: &str) -> Result<()>;
    async fn resume(&self, torrent_id: &str) -> Result<()>;
    async fn add_magnet(&self, magnet: &str, options: Options) -> Result<String>;
    async fn remove_torrent(&self, torrent_id: &str, remove_data: bool) -> Result<()>;
    async fn torrent(&self, torrent_id: &str) -> Result<Torrent>;
}
