
use super::Result;
use crate::{Torrent, TorrentBackend};
use deluge_rpc::{Session, InfoHash, TorrentOptions, Query, TorrentState};
use async_trait::async_trait;
use tokio::net::ToSocketAddrs;

pub struct DelugeBackend {
    session: Session,
}

#[derive(Clone, Debug, serde::Deserialize, Query)]
struct DelugeTorrent {
    hash: InfoHash,
    name: String,
    state: TorrentState,
    total_size: u64,
    progress: f32,
    upload_payload_rate: u64,
    download_payload_rate: u64,
    label: String,
    owner: String,
    tracker_host: String,
    tracker_status: String,
}

impl Torrent<InfoHash> for DelugeTorrent {
    fn name(&self) -> &str {
        &self.name
    }

    fn id(&self) -> InfoHash {
        self.hash 
    }
}

impl DelugeBackend {
    pub async fn new<A: ToSocketAddrs>(address: A, username: Option<&str>, password: Option<&str>) -> Result<Self> {
        println!("Creating new deluge backend");
        let mut session = Session::connect(address).await?;
        println!("Connected");
        if let (Some(username), Some(password)) = (username, password) {
            println!("Logging in");
            session.login(username, password).await?;
            println!("Logged in");
        }

        Ok(Self { session })
    }
}

#[async_trait]
impl TorrentBackend<InfoHash, DelugeTorrent> for DelugeBackend {
    async fn list(&self) -> Result<Vec<InfoHash>> {
        self.session.get_session_state().await.map_err(|e| e.into())
    }

    async fn pause(&self, torrent_id: InfoHash) -> Result<()> {
        self.session.pause_torrent(torrent_id).await.map_err(|e| e.into())
    }

    async fn resume(&self, torrent_id: InfoHash) -> Result<()> {
        self.session.resume_torrent(torrent_id).await.map_err(|e| e.into())
    }

    async fn add_magnet(&self, magnet: &str) -> Result<InfoHash> {
        // TODO Implement torrent options interface
        self.session.add_torrent_magnet(magnet, &TorrentOptions::default()).await.map_err(|e| e.into())
    }

    async fn remove_torrent(&self, torrent_id: InfoHash, remove_data: bool) -> Result<()> {
        self.session.remove_torrent(torrent_id, remove_data).await.map_err(|e| e.into()).map(|_| ())
    }

    async fn torrent(&self, torrent_id: InfoHash) -> Result<DelugeTorrent> {
        self.session.get_torrent_status::<DelugeTorrent>(torrent_id).await.map_err(|e| e.into())
    }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[tokio::test]
    async fn test_deluge() -> Result<()> {
        let backend = DelugeBackend::new(("", 58846), Some("deluge"), Some("deluge")).await?;
        println!("{:#?}", backend.list().await?);
        Ok(())
    }
}
