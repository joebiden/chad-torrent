pub mod backend;
pub mod torrent;

pub use backend::{DelugeBackend, QBittorrentBackend, TorrentBackend, TorrentClient};
pub use torrent::{Options, Torrent};
